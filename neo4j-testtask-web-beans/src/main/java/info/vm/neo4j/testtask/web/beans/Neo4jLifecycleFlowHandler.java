package info.vm.neo4j.testtask.web.beans;

import java.io.Serializable;

import dev.ritmico.faces.spi.conversation.LifecycleFlowHandler;

public class Neo4jLifecycleFlowHandler implements LifecycleFlowHandler, Serializable {

	private static final long serialVersionUID = 5472573538053398578L;

	@Override
	public <T> void beforeFinish(T instance, Class<T> entityClass, boolean managed) {

	}
	
}