package info.vm.neo4j.testtask.web.beans;

public enum FilterMode {
	
	ALL("MATCH (n)-[r]->(m) RETURN n,r,m"),
	
	NODES_ONLY("MATCH(n) RETURN n"),
	
	PERSONS_ONLY("MATCH(p:Person) RETURN p"),
	
	LOCATIONS_ONLY("MATCH(loc:Location) RETURN loc");
	
	String query;
	
	FilterMode(String query) {
		this.query = query;
	}

	public String getQuery() {
		return query;
	}

}