package info.vm.neo4j.testtask.web.beans;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import info.vm.neo4j.testtask.manager.DbManager;

@ViewScoped
@Named("indexWebBean")
public class IndexWebBean implements Serializable {

	private static final long serialVersionUID = 218446441575340589L;
	
	@Inject
	private DbManager dateNodeManager;
	
	private FilterMode filterMode;
	
	public void select() {
		filterMode = FilterMode.ALL;
	}
	
	public void selectFilterMode(AjaxBehaviorEvent event) {
		String fmStr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("form:filter-mode");
		filterMode = FilterMode.valueOf(fmStr);
	}
	
	public void createNode(AjaxBehaviorEvent event) {
//		dateNodeManager.createNode(Map.of("First", ""));
	}
	
	public String getQuery() {
		return filterMode.getQuery();
	}

}