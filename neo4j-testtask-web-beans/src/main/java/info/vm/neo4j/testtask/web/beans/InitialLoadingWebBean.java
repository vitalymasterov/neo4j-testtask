package info.vm.neo4j.testtask.web.beans;

import java.io.Serializable;

import javax.enterprise.event.Event;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import info.vm.neo4j.testtask.commons.events.InitialDataLoadingEvent;
import info.vm.neo4j.testtask.commons.events.InitialNodesLoadingEvent;
import info.vm.neo4j.testtask.commons.events.InitialRelationshipsLoadingEvent;

@Named("initialLoadingWebBean")
public class InitialLoadingWebBean implements Serializable {

	private static final long serialVersionUID = -8858234217653932531L;

	@Inject
	private Event<InitialDataLoadingEvent> dataLoadingEvent;
	
	@Inject
	private Event<InitialNodesLoadingEvent> nodesLoadingEvent;
	
	@Inject
	private Event<InitialRelationshipsLoadingEvent> relationshipsLoadingEvent;
	
	public void loadAllData(AjaxBehaviorEvent event) {
		dataLoadingEvent.fire(new InitialDataLoadingEvent());
	}
	
	public void loadNodesOnly(AjaxBehaviorEvent event) {
		nodesLoadingEvent.fire(new InitialNodesLoadingEvent());
	}
	
	public void loadRelationshipsOnly(AjaxBehaviorEvent event) {
		relationshipsLoadingEvent.fire(new InitialRelationshipsLoadingEvent());
	}
	
}