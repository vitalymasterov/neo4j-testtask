package info.vm.neo4j.testtask.web.beans;

import java.io.Serializable;

import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import info.vm.neo4j.testtask.manager.DbManager;

@ViewScoped
@Named("cleaningWebBean")
public class CleaningWebBean implements Serializable {
	
	private static final long serialVersionUID = -1772246802343027220L;
	
	@Inject
	private DbManager dbManager;
	
	public void clean(AjaxBehaviorEvent event) {
		dbManager.clean();
	}

}