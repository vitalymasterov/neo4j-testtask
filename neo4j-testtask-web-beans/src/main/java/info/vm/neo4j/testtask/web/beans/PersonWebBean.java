package info.vm.neo4j.testtask.web.beans;

import java.io.Serializable;
import java.util.Map;

import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import info.vm.neo4j.testtask.manager.DbManager;

@ViewScoped
@Named("personWebBean")
public class PersonWebBean implements Serializable {

	private static final long serialVersionUID = 1705364412466172700L;
	
	private static final String CREATE_NODE_QUERY = "CREATE (p:Person {Name: $Name, firstName : $firstName, lastName : $lastName, middleName : $middleName, birthdate : $birthdate, deathdate : $deathdate})";
	
	@Inject
	private DbManager dbManager;
	
	private String firstName;
	
	private String lastName;
	
	private String middleName;
	
	private String birthdate;
	
	private String deathdate;
		
	public void create(AjaxBehaviorEvent evnt) {
		String displayName = firstName + " " + lastName;
		Map<String, Object> props = Map.of(
				"firstName", firstName,
				"lastName", lastName,
				"middleName", middleName,
				"birthdate", birthdate,
				"deathdate", deathdate,
				"Name", displayName
		);
		
		dbManager.begin();
		dbManager.execute(CREATE_NODE_QUERY, props);
		dbManager.commit();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getDeathdate() {
		return deathdate;
	}

	public void setDeathdate(String deathdate) {
		this.deathdate = deathdate;
	}
	
}