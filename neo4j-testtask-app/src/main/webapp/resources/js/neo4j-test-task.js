var vm = {

	"neo4j": {

		"drawGraphs": function() {
			var query = $("#form\\:cypher").get(0).value;
			var config = {
				container_id: "viz",
				server_url: "bolt://77.235.46.3:7687",
				server_user: "neo4j",
				server_password: "Xai2bez9",
				arrows: true,
				labels: {
					"Person": {
						caption: "Name",
						size: "pagerank",
						community: "community"
					},
					"Location": {
						caption: "Name",
						size: "pagerank",
						community: "community"
					}					
				},
				relationships: {
					"RETWEETS": {
						caption: false,
						thickness: "count"
					}
				},
				initial_cypher: query
			}

			var viz = new NeoVis.default(config);
			viz.render();			
		},

		"redrawGraphs": function(event) {
			if (event.status != 'success') {
				return;
			}
			
			vm.neo4j.drawGraphs();
		}

	}

} 