package info.vm.neo4j.testtask.service.impl;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import info.vm.neo4j.testtask.commons.events.InitialDataLoadingEvent;
import info.vm.neo4j.testtask.commons.events.InitialNodesLoadingEvent;
import info.vm.neo4j.testtask.commons.events.InitialRelationshipsLoadingEvent;
import info.vm.neo4j.testtask.manager.DbManager;
import info.vm.neo4j.testtask.service.DataNodesService;

@Stateless
public class DataNodesServiceImpl implements DataNodesService, Serializable {
	
	private static final long serialVersionUID = -9222746015055032488L;
	
	@Inject
	private DbManager dbManager;

	@Inject
	private Event<InitialDataLoadingEvent> dataLoadingEvent;
	
	@Inject
	private Event<InitialNodesLoadingEvent> nodesLoadingEvent;
	
	@Inject
	private Event<InitialRelationshipsLoadingEvent> relationshipsLoadingEvent;	
	
	public void clean() {
		dbManager.clean();
	}

	@Override
	public void initialDataLoading() {
		dataLoadingEvent.fire(new InitialDataLoadingEvent());
	}

	@Override
	public void initialNodesLoading() {
		nodesLoadingEvent.fire(new InitialNodesLoadingEvent());
	}

	@Override
	public void initialRelationshipsLoading() {
		relationshipsLoadingEvent.fire(new InitialRelationshipsLoadingEvent());
	}	 

}