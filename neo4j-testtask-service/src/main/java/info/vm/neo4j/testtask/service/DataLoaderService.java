package info.vm.neo4j.testtask.service;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Local
@Path("/data/loader")
public interface DataLoaderService {
	
    @GET
    @Path("/initial")
    @Produces(MediaType.APPLICATION_JSON)	
	public Response initialLoading();

}