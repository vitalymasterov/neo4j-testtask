package info.vm.neo4j.testtask.service.impl;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import info.vm.neo4j.testtask.commons.events.InitialDataLoadingEvent;
import info.vm.neo4j.testtask.service.DataLoaderService;

@Stateless
public class DataLoaderServiceImpl implements DataLoaderService, Serializable {

	private static final long serialVersionUID = 7713367603827292413L;

	@Inject
	private Event<InitialDataLoadingEvent> initialLoadingEvent;
	
	@Override
	public Response initialLoading() {
		initialLoadingEvent.fire(new InitialDataLoadingEvent());
		return Response
				.ok()
				.build();
	}

}