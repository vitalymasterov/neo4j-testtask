package info.vm.neo4j.testtask.service;

import javax.ejb.Local;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Local
@Path("/data/nodes")
public interface DataNodesService {
	
    @DELETE
    @Path("/clean")
    @Produces(MediaType.APPLICATION_JSON)		
	public void clean();
    
    @GET
    @Path("/initial/loading/all")
    @Produces(MediaType.APPLICATION_JSON)		
	public void initialDataLoading();

    @GET
    @Path("/initial/loading/nodes")
    @Produces(MediaType.APPLICATION_JSON)		
	public void initialNodesLoading();
    
    @GET
    @Path("/initial/loading/relationships")
    @Produces(MediaType.APPLICATION_JSON)		
	public void initialRelationshipsLoading();
    
}