package info.vm.neo4j.testtask.manager;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.config.ConfigProperty;
import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.Session;
import org.neo4j.driver.Transaction;
import org.neo4j.driver.Result;

@RequestScoped
public class DbManager implements Serializable {

	private static final long serialVersionUID = 8339665398965717495L;
	
	private static final String CLEAN_QUERY = "MATCH (n) DETACH DELETE n;";
	
	/**
	 * Параметр "строка подключения" к базе данных из конфигурационного файла
	 */
	@Inject @ConfigProperty(name = "neo4j.db.uri")
	private String uri;
	
	/**
	 * Параметр "имя пользователя" к базе данных из конфигурационного файла
	 */
	@Inject @ConfigProperty(name = "neo4j.db.username")
	private String username;

	/**
	 * Параметр "пароль" к базе данных из конфигурационного файла
	 */
	@Inject @ConfigProperty(name = "neo4j.db.password")
	private String password;
	
	private Driver driver;
	
	private Session session;
	
	private Transaction tx;
	
	@PostConstruct
	private void postConstruct() {
		driver = GraphDatabase.driver(uri, AuthTokens.basic(username, password));
		session = driver.session();
	}
	
	@PreDestroy
	private void preDestroy() {
		session.close();
		driver.close();
	}
	
	public void clean() {
		tx = session.beginTransaction();
		tx.run(CLEAN_QUERY);
		tx.commit();
	}
	
	public void begin() {
		tx = session.beginTransaction();
	}
	
	public void commit() {
		tx.commit();
	}

	public void execute(String query, Map<String, Object> props) {
		tx.run(query, props);
	}
	
	public void createNode(String query, Map<String, Object> props) {
		tx.run(query, props);
	}
	
	public Long matchSingle(String nodeLabel, String property, String pattern) {
		if (StringUtils.isEmpty(pattern)) {
			return null;
		}
		
		String query = String.format("MATCH (node:%s) where node.%s = $%s RETURN node", nodeLabel, property, property);
		Result rs = tx.run(query, Map.of(property, pattern.trim()));
		Long id = rs.next().fields().get(0).value().asNode().id();
		return id;
	}
	
	public Long matchSingle(String query, Map<String, Object> props) {
		Result rs = tx.run(query, props);
		return rs.next().fields().get(0).value().asNode().id();
	}
	
	/*
	public void resultList() {
		session.writeTransaction(new TransactionWork<String>() {
			
			@Override
			public String execute(Transaction tx) {
				Result rs = tx.run("MATCH (a:Artist) RETURN a");
				while (rs.hasNext()) {
					org.neo4j.driver.Record rec = rs.next();
				}
				return null;
			}
			
		});
	}
	*/	
	
}