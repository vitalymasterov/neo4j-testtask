package info.vm.neo4j.testtask.commons.events;

import java.io.Serializable;

/**
 * Событие, оповещающее о необходимости провести первоначальную только связей.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class InitialRelationshipsLoadingEvent implements Serializable {

	private static final long serialVersionUID = -7276392809003549722L;

}