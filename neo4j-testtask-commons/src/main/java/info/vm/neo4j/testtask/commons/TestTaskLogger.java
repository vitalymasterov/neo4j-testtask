package info.vm.neo4j.testtask.commons;

import static org.jboss.logging.Logger.Level.TRACE;

import org.jboss.logging.BasicLogger;
import org.jboss.logging.Logger;
import org.jboss.logging.annotations.LogMessage;
import org.jboss.logging.annotations.Message;
import org.jboss.logging.annotations.MessageLogger;

@MessageLogger(projectCode = "TESTTASK")
public interface TestTaskLogger extends BasicLogger {

	TestTaskLogger LOGGER = Logger.getMessageLogger(TestTaskLogger.class, TestTaskLogger.class.getPackage().getName());

	@LogMessage(level = TRACE)
	@Message(id = 1, value = "Попытка загрузить первоначальные данные из файлов '%s'")	
	void tryLoadingInitialData(String dbxFilePath);

	@LogMessage(level = TRACE)
	@Message(id = 2, value = "Певроначальные данные для успешно загружены в БД")	
	void successLoadingInitialData();
	
}