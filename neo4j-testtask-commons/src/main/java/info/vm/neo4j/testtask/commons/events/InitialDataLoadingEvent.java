package info.vm.neo4j.testtask.commons.events;

import java.io.Serializable;

/**
 * Событие, оповещающее о необходимости провести первоначальную загрузку узлов и связей.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class InitialDataLoadingEvent implements Serializable {
	
	private static final long serialVersionUID = 9176481541753352239L;

}