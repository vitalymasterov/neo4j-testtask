package info.vm.neo4j.testtask.commons.events;

import java.io.Serializable;

/**
 * Событие, оповещающее о необходимости провести первоначальную загрузку только узлов.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class InitialNodesLoadingEvent implements Serializable {

	private static final long serialVersionUID = -4541819295965013252L;

}