package info.vm.neo4j.testtask.loader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Paths;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.deltaspike.core.api.config.ConfigProperty;

import info.vm.neo4j.testtask.commons.events.InitialDataLoadingEvent;
import info.vm.neo4j.testtask.commons.events.InitialNodesLoadingEvent;
import info.vm.neo4j.testtask.commons.events.InitialRelationshipsLoadingEvent;
import info.vm.neo4j.testtask.loader.process.FileDataLoader;
import info.vm.neo4j.testtask.loader.xml.ItemsXml;
import info.vm.neo4j.testtask.loader.xml.LocationXml;
import info.vm.neo4j.testtask.loader.xml.PersonXml;
import info.vm.neo4j.testtask.manager.DbManager;

public class InitialDataLoader implements Serializable {

	private static final long serialVersionUID = -42826606487406429L;
	
	private static final String LOCATIONS_XML = "locations.xml";
	
	private static final String PERSONS_XML = "persons.xml";
	
	private static final Class<?>[] JAXB_CLASSES = new Class[] {ItemsXml.class, PersonXml.class, LocationXml.class};
	
	@Inject @ConfigProperty(name = "initial.data.dir")
	private String initialDataDir;
		
	@Inject @Named("locations")
	private FileDataLoader<LocationXml> locationsLoader;
	
	@Inject @Named("persons")
	private FileDataLoader<PersonXml> personsLoader;
	
	@Inject
	private DbManager dbManager;
	
	public void loadAllData(@Observes InitialDataLoadingEvent event) {
		dbManager.begin();
		
		ItemsXml<LocationXml> locations = loadXmlFile(LocationXml.class, LOCATIONS_XML);		
		ItemsXml<PersonXml> persons = loadXmlFile(PersonXml.class, PERSONS_XML);
		
		locationsLoader.loadNodes(locations);
		personsLoader.loadNodes(persons);
		
		locationsLoader.loadRelationships(locations);
		personsLoader.loadRelationships(persons);
		
		dbManager.commit();
	}

	public void loadNodesOnly(@Observes InitialNodesLoadingEvent event) {
		dbManager.begin();
		
		ItemsXml<LocationXml> locations = loadXmlFile(LocationXml.class, LOCATIONS_XML);		
		ItemsXml<PersonXml> persons = loadXmlFile(PersonXml.class, PERSONS_XML);
		
		locationsLoader.loadNodes(locations);
		personsLoader.loadNodes(persons);
		
		dbManager.commit();
	}
	
	public void loadRelationshipsOnly(@Observes InitialRelationshipsLoadingEvent event) {
		dbManager.begin();
		
		ItemsXml<LocationXml> locations = loadXmlFile(LocationXml.class, LOCATIONS_XML);		
		ItemsXml<PersonXml> persons = loadXmlFile(PersonXml.class, PERSONS_XML);
		
		locationsLoader.loadRelationships(locations);
		personsLoader.loadRelationships(persons);
		
		dbManager.commit();
	}	
	
	@SuppressWarnings("unchecked")
	private <T> ItemsXml<T> loadXmlFile(Class<T> jaxbClass, String fileName) {
        try (
        		FileInputStream fis = new FileInputStream(Paths.get(initialDataDir, fileName).toFile());
        ) {
            JAXBContext jaxbContext = JAXBContext.newInstance(JAXB_CLASSES);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        	return (ItemsXml<T>)unmarshaller.unmarshal(fis);
        } catch(JAXBException e) {
        	throw new RuntimeException(e);
        } catch(IOException e) {
        	throw new RuntimeException(e);	
        }
	}
	
}