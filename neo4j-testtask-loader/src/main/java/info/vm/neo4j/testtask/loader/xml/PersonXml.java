package info.vm.neo4j.testtask.loader.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Представление полей персоны, которое используется 
 * в JAXB-трансофрмациях при считывании данных о персоне из xml-файла.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 */
@XmlRootElement(name = "person")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PersonXml implements Serializable {

	private static final long serialVersionUID = 4183011794569034166L;
	
	private String firstName;
	
	private String lastName;
	
	private String middleName;
	
	private String birthdate;
	
	private String deathdate;
	
	private String birthLocation;
	
	private String deathLocation;

	@XmlElement(name = "first-name")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@XmlElement(name = "last-name")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@XmlElement(name = "middle-name")
	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@XmlElement(name = "birthdate")
	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	@XmlElement(name = "deathdate")
	public String getDeathdate() {
		return deathdate;
	}
	
	public void setDeathdate(String deathdate) {
		this.deathdate = deathdate;
	}

	@XmlElement(name = "birth-location")
	public String getBirthLocation() {
		return birthLocation;
	}
	
	public void setBirthLocation(String birthLocation) {
		this.birthLocation = birthLocation;
	}

	@XmlElement(name = "death-location")
	public String getDeathLocation() {
		return deathLocation;
	}

	public void setDeathLocation(String deathLocation) {
		this.deathLocation = deathLocation;
	}
	
}