package info.vm.neo4j.testtask.loader.process;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import info.vm.neo4j.testtask.loader.xml.ItemsXml;
import info.vm.neo4j.testtask.loader.xml.LocationXml;
import info.vm.neo4j.testtask.manager.DbManager;

@Named("locations")
public class LocationsFileLoader implements FileDataLoader<LocationXml>, Serializable {

	private static final long serialVersionUID = -7406520687945802696L;
	
	private static final String QUERY = "CREATE (loc:Location {Name: $Name, name : $name})";
	
	private static final String DISPLAY_NAME = "Name";
	
	private static final String NAME = "name";
		
	@Inject
	private DbManager dbManager;
	
	@Override
	public void loadNodes(ItemsXml<LocationXml> itemsXml) {
		for (LocationXml locationXml : itemsXml.getItems()) {
			Map<String, Object> props = new HashMap<>();
			props.put(DISPLAY_NAME, locationXml.getName().trim());
			props.put(NAME, locationXml.getName().trim());
			dbManager.createNode(QUERY, props);
		}
	}

	@Override
	public void loadRelationships(ItemsXml<LocationXml> itemsXml) {
		
	}

}