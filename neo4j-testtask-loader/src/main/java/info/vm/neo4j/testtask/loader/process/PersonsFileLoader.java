package info.vm.neo4j.testtask.loader.process;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import info.vm.neo4j.testtask.loader.xml.ItemsXml;
import info.vm.neo4j.testtask.loader.xml.PersonXml;
import info.vm.neo4j.testtask.manager.DbManager;

@Named("persons")
public class PersonsFileLoader implements FileDataLoader<PersonXml>, Serializable {

	private static final long serialVersionUID = -5508198622162050635L;
	
	private static final String CREATE_NODE_QUERY = "CREATE (p:Person {Name: $Name, firstName : $firstName, lastName : $lastName, middleName : $middleName, birthdate : $birthdate, deathdate : $deathdate})";
	
	private static final String CREATE_BIRTH_LOCATION_RELATIONSHIP_QUERY = "MATCH (p:Person),(loc:Location) " 
			+ "WHERE p.firstName = $firstName and p.lastName = $lastName and  loc.name = $location " 
			+ "CREATE (p)-[rel:birthLocation]->(loc) "
			+ "RETURN rel";

	private static final String CREATE_DEATH_LOCATION_RELATIONSHIP_QUERY = "MATCH (p:Person),(loc:Location) " 
			+ "WHERE p.firstName = $firstName and p.lastName = $lastName and  loc.name = $location " 
			+ "CREATE (p)-[rel:deathLocation]->(loc) "
			+ "RETURN rel";
	
	private static final String DISPLAY_NAME = "Name";
	
	private static final String FIRST_NAME = "firstName";
	
	private static final String LAST_NAME = "lastName";
	
	private static final String MIDDLE_NAME = "middleName";
	
	private static final String BIRTHDATE = "birthdate";
	
	private static final String DEATHDATE = "deathdate";
		
	@Inject
	private DbManager dbManager;
	
	@Override
	public void loadNodes(ItemsXml<PersonXml> itemsXml) {
		for (PersonXml personXml : itemsXml.getItems()) {			
			Map<String, Object> props = new HashMap<>();
			String displayName = personXml.getFirstName().trim() + " " + personXml.getLastName().trim();
			props.put(DISPLAY_NAME, displayName);
			props.put(FIRST_NAME, personXml.getFirstName().trim());
			props.put(LAST_NAME, personXml.getLastName().trim());
			props.put(MIDDLE_NAME, personXml.getMiddleName().trim());
			props.put(BIRTHDATE, personXml.getBirthdate().trim());
			if (personXml.getDeathdate() != null) {
				props.put(DEATHDATE, personXml.getDeathdate().trim());
			}
			
			dbManager.createNode(CREATE_NODE_QUERY, props);
		}		
	}

	@Override
	public void loadRelationships(ItemsXml<PersonXml> itemsXml) {
		for (PersonXml personXml : itemsXml.getItems()) {
			if (personXml.getBirthLocation() == null || personXml.getDeathLocation() == null) {
				continue;
			}
			
			Map<String, Object> props = Map.of(
					"firstName", personXml.getFirstName().trim(), 
					"lastName", personXml.getLastName().trim(),
					"location", personXml.getBirthLocation().trim());			
			dbManager.execute(CREATE_BIRTH_LOCATION_RELATIONSHIP_QUERY, props);
			
			props = Map.of(
					"firstName", personXml.getFirstName().trim(), 
					"lastName", personXml.getLastName().trim(),
					"location", personXml.getDeathLocation().trim());			
			dbManager.execute(CREATE_DEATH_LOCATION_RELATIONSHIP_QUERY, props);
		}
	}

}