package info.vm.neo4j.testtask.loader.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

public class LocationsXml extends ItemsXml<LocationXml> implements Serializable {

	private static final long serialVersionUID = -5480583749262923893L;
	
}