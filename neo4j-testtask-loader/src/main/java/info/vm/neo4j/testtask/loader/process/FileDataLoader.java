package info.vm.neo4j.testtask.loader.process;

import info.vm.neo4j.testtask.loader.xml.ItemsXml;

public interface FileDataLoader<T> {
	
	public void loadNodes(ItemsXml<T> itemsXml);
	
	public void loadRelationships(ItemsXml<T> itemsXml);

}