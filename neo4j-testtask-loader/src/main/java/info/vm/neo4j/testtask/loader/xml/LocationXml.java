package info.vm.neo4j.testtask.loader.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Представление полей географической локации, которое используется 
 * в JAXB-трансофрмациях при считывании данных о персоне из xml-файла.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 */
@XmlRootElement(name = "location")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class LocationXml implements Serializable {

	private static final long serialVersionUID = 3264783332236784367L;
	
	private String name;

	@XmlElement(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}