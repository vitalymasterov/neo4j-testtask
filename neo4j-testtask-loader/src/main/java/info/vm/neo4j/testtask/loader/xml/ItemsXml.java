package info.vm.neo4j.testtask.loader.xml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement(name = "data")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlSeeAlso({PersonsXml.class, LocationsXml.class})
public class ItemsXml<T> implements Serializable {

	private static final long serialVersionUID = -8652458280509805039L;
	
	private List<T> items;
	
	public ItemsXml() {
		items = new ArrayList<>();
	}

	@XmlElementWrapper(name = "items")
	@XmlAnyElement(lax = true)
	public List<T> getItems() {
		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}

}